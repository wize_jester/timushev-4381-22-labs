﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{
    class Triangle
    {
        private static int a, b, c, p;
        private static int[] line;
        
        public Triangle()
        {
            line = new int[3] { A, B,C };
        }
        public int this[int i]
        {
            set { line[i] = value; }
            get { return line[i]; }

        }
        public static int A
        {
            set { a = A; }
            get { return a; }
        }
        public static int B
        {
            set { b = B; }
            get { return b; }
        }
        public static int C
        {
            set { c = C; }
            get { return c; }
        }
        public bool Real
        {
            get
            {
                if (a + b > c & a + c > b & b + c > a)
                    return true;
                else return false;
            }
        }
        public void Create(int A, int B, int C)
        {
            Triangle.A = A;
            Triangle.B = B;
            Triangle.C = C;
        }
        public void Write()
        { Console.WriteLine("треугольник имеет стороны ({0};{1};{2})", a, b, c); }
        public int Perimetr()
        {
            p = a + b + c;
            return (p);
        }
        public double S()
        {
            int p = Triangle.p;
            double s = Math.Sqrt(p / 2 * (p / 2 - a) * (p / 2 - b) * (p / 2 - c));
            return (s);
        }
        public static Triangle operator ++(Triangle line)
        {
            Triangle.line[0] += 1;
            Triangle.line[1] += 1;
            Triangle.line[2] += 1;
            return line;

        }
        public static Triangle operator --(Triangle line)
        {
            Triangle.line[0] -= 1;
            Triangle.line[1] -= 1;
            Triangle.line[2] -= 1;
            return line;
        }
       
        public static Triangle operator *(Triangle line, int k)
        {
            Triangle.line[0] *= k;
            Triangle.line[1] *= k;
            Triangle.line[2] *= k;
            return line;
        }
        
    }
}
