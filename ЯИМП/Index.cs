﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{
    class Index
    {
        private object[] index;
       
        public Index(ref int x,ref int y)
        {
            index = new object[2];
            index[0] = x;
            index[1] = y;
        }
       
        public object this[int j]
        {
            set
            {
                index[j-1] = value;
            }
            get
            {
                return index[j];
            }
        }
    }
}
