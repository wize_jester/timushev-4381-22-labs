﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{
    class Data
    {
        private DateTime data;
        private int year;
        private int month;
        private int day;
        public DateTime Date
        {
            set { data = value; }
            get { return data; }
        }
        public string Create()
        {
            Date = new DateTime(2009,01,1);
            return Date.ToString("D");
        }
        public string Create(string s)
        {
            int[] d = new int[3];
            char[] split = new char[] { ',', '.', ' ', '/', '-' };

            string[] ar = s.Split(split);
            for (int i = 0; i < 3; i++)
                d[i] = Convert.ToInt32(ar[i]);
            year = d[2];
            month = d[1];
            day = d[0];

            Date = new DateTime(year, month, day);
            return Date.ToString("D");
        }
        public string PrDate()
        {
            DateTime prdate = new DateTime();
            prdate = Date.AddDays(-1);
            return prdate.ToString("D");
        }
        public string NxDate()
        {
            DateTime nxdate = new DateTime();
            nxdate = Date.AddDays(1);
            return nxdate.ToString("D");
        }
        public Tuple<string,string> Wis()
        {
            string wis="не ";
            string add="";
            if (Tr(year) == true)
                wis = "";
            if (wis == "не ")
            {
                int ae = 0;
                string Ae ="";
                if (Tr(year - 2) == true & Tr(year + 2) == true)
                {
                    if (month < 6)
                    {
                        ae = year - 2;
                        Ae = Convert.ToString(ae);
                    }
                    else if (month == 6 & day == 30)
                    {
                        string y1 = Convert.ToString(year - 2);
                        string y2 = Convert.ToString(year + 2);
                        Ae = y1 + " и " + y2;

                    }
                    else if (month == 6 & day < 30)
                    {
                        ae = year - 2;
                        Ae = Convert.ToString(ae);
                    }
                    else
                    {
                        ae = year + 2;
                        Ae = Convert.ToString(ae);
                    }
                    }
                else for (int i = -3; i <= 3; i++)
                        if (Tr(year + i) == true)
                        {
                            ae = year + i;
                            Ae = Convert.ToString(ae);
                            break;
                        }
                
                add = (",ближайший високосный год - "+Ae);
            }
                return Tuple.Create(add,wis);    
            
        }
        private bool Tr(int year)
        {
            bool tr = false;
            if (year % 400 == 0)
                tr = true;
            else if (year % 100 == 0)
                tr = false;
            else if (year % 4 == 0)
                tr = true;
            else tr = false;
            return tr;
        }
        public int End()
        {
            TimeSpan L;
            DateTime monthend=new DateTime(year,month,DateTime.DaysInMonth(year,month));
            L = monthend - Date;
            int l = (int)L.TotalDays;
            return l;
        }
    }
}
