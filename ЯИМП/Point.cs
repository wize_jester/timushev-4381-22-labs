﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{
    class Point
    {

        private static int x = 0;
        private static int y = 0;
        private static int scal;
        int[] cor;
        public Point()
        { }

        public Point(int size)
        {
             cor= new int[2]{ X, Y };
        }


        public int this[int i]
        {

           get{
                if (i == 0 || i == 1)
                    return cor[i];
                else { Console.WriteLine("неверный индекс");
                    return -999999999; }
            }
           set{
                if (i == 1 || i == 0)
                    cor[i] = value;
            }
            
            
        }
        
        public static int X
        {
            set { x = value; }
            get { return x; }
        }
        public static int Y
        {
            set { y = value;}
            get { return y; }
        }
        public int Scal
        {
            set
            {
                scal = Convert.ToInt32(Console.ReadLine());
                X *= scal;
                Y *= scal;
            }
        }
        public void Dot(int X, int Y)
        {
            Point.X = X;
            Point.Y = Y;
        }
        public void Dot()
        {
            X = 0;
            Y = 0;
        }
        public void Write()
        { Console.WriteLine("точка имеет координаты ({0};{1})", X, Y); }
        public double S()
        {
            double r = Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
            return (r);
        }
        public void Vector(int X, int Y)
        {
            Point.X += X;
            Point.Y += Y;

        }
        public static Point operator ++(Point cor)
        {
            cor[0] += 1;
            cor[1] += 1;
            return cor;

        }
        public static Point operator --(Point cor)
        {
            cor[0] -= 1;
            cor[1] -= 1;
            return cor;
        }
        public static bool operator true(Point cor)
        {
            return cor[0] == cor[1];
        }
        public static bool operator false(Point cor)
        {
            return cor[0] != cor[1];
        }
        
        public static Point operator +(Point cor, int k)
        {
            cor[0] += k;
            cor[1] += k;
            return cor;
        }
    }
}

