﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("1. Точка\n" +
                "2. Треугольник\n" +
                "3. прямоугольник\n" +
                "4. Деньги\n" +
                "5. Массив\n" +
                "6. Двумерный массив\n" +
                "7. Строки\n" +
                "8. Дата\n" +
                "Введите номер задачи: ");
            int l = Convert.ToInt32(Console.ReadLine());
            switch (l)
            {
                case 1:
                    {
                        Point P = new Point();
                        int x;
                        int y;
                        Console.WriteLine("Введите координаты точки ");
                        x = Convert.ToInt32(Console.ReadLine());
                        y = Convert.ToInt32(Console.ReadLine());
                        if (x != 0 & y != 0)
                            P.Dot(x, y);
                        else P.Dot();
                        P.Write();
                        double r = P.S();
                        Console.WriteLine("расстояние от начала координат до точки = {0}", r);
                        Console.WriteLine("Введите координаты вектора ");
                        x = Convert.ToInt32(Console.ReadLine());
                        y = Convert.ToInt32(Console.ReadLine());
                        P.Vector(x, y);
                        P.Write();
                        Point a = new Point(2);
                        int i = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(a[i]);
                        a[i] = i * i;
                        Console.WriteLine(a[i]);
                    }
                    break;
                case 2:
                    {

                        Triangle T = new Triangle();
                        int a = 0;
                        int b = 0;
                        int c = 0;

                        Console.WriteLine("Введите длины сторон");
                        a = Convert.ToInt32(Console.ReadLine());
                        b = Convert.ToInt32(Console.ReadLine());
                        c = Convert.ToInt32(Console.ReadLine());
                        if (T.Real == true)
                        {
                            T.Create(a, b, c);
                            T.Write();

                            int p = T.Perimetr();
                            Console.WriteLine("периметр треугольника ={0}", p);
                            double s = T.S();
                            Console.WriteLine("площадь треугольника ={0}", s);
                        }
                        else Console.WriteLine("невозможно построить треугольник со сторонам {0},{1},{2}", a, b, c);

                    }
                    break;

                case 3:
                    {
                        Rectangle R = new Rectangle();
                        

                        int a = 0;
                        int b = 0;
                        Console.WriteLine("Введите длины сторон");
                        a = Convert.ToInt32(Console.ReadLine());
                        b = Convert.ToInt32(Console.ReadLine());
                        if (a == b)
                            R.Name("Квадрат");
                        else R.Name("Прямоугольник");
                        R.Create(a, b);
                        R.Write();
                        string name = R.name;
                        int p = R.Perimetr();
                        Console.WriteLine("периметр {1}а ={0}", p, name);
                        int s = R.S();
                        Console.WriteLine("площадь {1}а = {0}", s, name);




                    }
                    break;
                case 4:
                    {
                        Money M = new Money();
                        Console.WriteLine("Введите номинал");
                        int first = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Введите количество");
                        int second = Convert.ToInt32(Console.ReadLine());
                        M.Create(first, second);
                        M.Write();

                        Console.WriteLine("Введите стоимость товара");
                        int price = Convert.ToInt32(Console.ReadLine());
                        M.Buy(price);

                        Console.WriteLine("Введите стоимость товара");
                        first = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Введите количество");
                        second = Convert.ToInt32(Console.ReadLine());
                        price = first * second;
                        M.Buy(price);
                    }
                    break;
                case 5:
                    {
                        Field F = new Field();
                        Console.Write("Введите длину массива: ");
                        int n = Convert.ToInt32(Console.ReadLine());
                        int[] a = F.Create(n);
                        string s = Console.ReadLine();
                        a=F.Fill(s);
                        Console.WriteLine(a.Length);
                        Console.WriteLine(a);
                        a=Field.Sort();
                        Console.Write("отсортированный массив: ");
                        F.Write();
                    }
                    break;
                case 6:
                    {
                        Field2 F = new Field2();
                        Console.Write("Введите размерность: ");
                        Field2.N = Convert.ToInt32(Console.ReadLine());
                        int f = Field2.N;
                        int[,] a = F.Create(Field2.N);
                        a=F.fill(a);
                        F.Write();
                        F.Bolt();
                        F.IntArray = a;
                        Console.WriteLine("Количество нулевых элементов: {0}",F.Zero);
                        Console.Write("Введите константу: ");
                        F.Diag= Convert.ToInt32(Console.ReadLine());
                        F.Write();
                    }
                    break;
                case 7:
                    {
                        STR str = new STR();
                        Console.WriteLine("Введите строку:");
                        str.Create();
                        Console.WriteLine("в строке содержится {0} пробелов",str.Space());
                        Console.WriteLine(str.ToLow());
                        Console.WriteLine(str.DeletePuncs());
                    }
                    break;
                case 8:
                    {
                        Console.Write("Введите дату в формате [День,Месяц,Год]: ");
                        string s=Console.ReadLine();
                        Data D = new Data();
                        if (s.Length!=0)
                        Console.WriteLine(D.Create(s));
                        else
                        Console.WriteLine(D.Create());
                        Console.WriteLine("Дата предыдущего дня: {0}",D.PrDate());
                        Console.WriteLine("Дата следующего дня: {0}",D.NxDate());
                        string add, wis;
                        var finish = D.Wis();
                        add = finish.Item1;
                        wis = finish.Item2;
                        Console.WriteLine("Год {0}является високосным{1}",wis,add);
                        Console.WriteLine("До конца месяца {0} дней",D.End());
                    }
                    break;
            }


             Console.ReadKey();
        }
        
        
    }


}