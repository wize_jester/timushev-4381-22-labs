﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{
    class Field
    {
        private static int[] IntArray;
        private static int N;
        public static int n
        {
            get { return N; }
        }
        public int this[int i]
        {
            set { IntArray[i]=value; }
            get{ return IntArray[i]; }
        }
        public int[] Create(int n)
        {
            IntArray = new int[n];
            N = n;
            return IntArray;
        }
        public int[] Fill(string s)
        {
            char[] split = new char[] { ',', '.', ' ', '/', '-' };

            string[] ar = s.Split(split);
            
            for (int i = 0; i <= s.Length - 1; i++)
            {
                IntArray[i] = Convert.ToInt32(ar[i]);
            }
            return IntArray;

        }
        public static int[] Sort()
        {

            Array.Sort(IntArray);
                return IntArray;
        }
        public void Write()
        {
            for (int i = 0; i < n; i++)
                Console.WriteLine(IntArray[i] + ' ');
        }
        public static Field operator ++(Field IntArray)
        {
            for (int i = 0; i < n; i++)
                Field.IntArray[i]+=1;

            return IntArray;

        }
        public static Field operator --(Field IntArray)
        {
            for (int i = 0; i < n; i++)
                Field.IntArray[i] -= 1;

            return IntArray;
        }
        public static bool operator !(Field IntArray)
        {
            
            if (IntArray.Equals(Sort()))
                return true;
            else
                return false;
        }
        public static Field operator *(Field IntArray, int k)
        {

            for (int i = 0; i < n; i++)
                Field.IntArray[i] *= k;

            return IntArray;
        }

    }
}
