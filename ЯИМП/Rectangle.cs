﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЯИМП
{
    class Rectangle
    {
        private int a, b, p;
        private static int[] line;

        public Rectangle(int a, int b)
        {
            A = a;
            B = b;
        }

        public Rectangle()
        {

        }
        public int this[int i]
        {
            set { line[i] = value; }
            get { return line[i]; }

        }
        public  int A
        {
            set { a = A; }
            get { return a; }
        }
        public int B
        {
            set { b = B; }
            get { return b; }
        }
        public void Sqare()
        {
                if (A == B)
                    Name( "Квадрат");
                else Name("Прямоугольник");
        
            

        }
        public String name;
        public void Name(string name)
        { this.name = name; }

        public void Create(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
        public void Write()
        { Console.WriteLine("{2} имеет стороны ({0};{1})", a, b, name); }
        public int Perimetr()
        {
            p = 2 * (a + b);
            return (p);
        }
        public int S()
        {
            int s = a * b;
            return (s);
        }  
        public static Rectangle operator ++(Rectangle line)
        {
            Rectangle.line[0] += 1;
            Rectangle.line[1] += 1;
            
            return line;

        }
        public static Rectangle operator --(Rectangle line)
        {
            Rectangle.line[0] -= 1;
            Rectangle.line[1] -= 1;
            
            return line;
        }
        public static Rectangle operator *(Rectangle line, int k)
        {
            Rectangle.line[0] *= k;
            Rectangle.line[1] *= k;
           
            return line;
        }

    }
}
