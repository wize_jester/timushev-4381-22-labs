﻿using System;

namespace Lab3
{
    class Hyp : Line
    {
        public Hyp(int a, int b, int x) : base(a, b, x) { }

        public override int Calc()
        {
            return A / X + B;
        }

        public override void Print()
        {
            Console.WriteLine("\nHyperbola\nА равно: {0} \nВ равно: {1} \nX равно: {2}", A, B, X);
            Console.WriteLine("Y равно: {0}", Calc());
        }
    }
}
