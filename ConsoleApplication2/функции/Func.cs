﻿namespace Lab3
{
    abstract class Funс
    {
        public int A { get; set; }
        public int B { get; set; }
        public int X { get; set; }

        abstract public int Calc();
        abstract public void Print();
    }
}
