﻿using System;

namespace Lab3
{
    //Электронный ресурс (название, фамилия автора, ссылка, аннотация)
    class EResource : Publisher
    {
        public string Link { get; set; }
        public string Annotation { get; set; }

        public EResource(string name, string author, string link, string annotation)
        {
            Name = name;
            Author = author;
            Link = link;
            Annotation = annotation;
        }

        public override void Print()
        {
            Console.WriteLine("\nЭлектронный ресурс\nНазвание: {0} \nАвтор: {1} \nСсылка: {2}, \nАннотация: {3}", Name, Author, Link, Annotation);
        }
    }
}
