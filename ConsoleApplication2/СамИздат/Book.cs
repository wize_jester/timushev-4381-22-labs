﻿using System;

namespace Lab3
{
    //Книга (название, фамилия автора, год издания, издательство)
    class Book : Publisher
    {
        public int Year { get; set; }
        public string Publisher { get; set; }

        public Book(string name, string author, int year, string publisher)
        {
            Name = name;
            Author = author;
            Year = year;
            Publisher = publisher;
        }

        public override void Print()
        {
            Console.WriteLine("\nКнига\nНазвание: {0} \nАвтор: {1} \nГод издания: {2} \nИздательство: {3}", Name, Author, Year, Publisher);
        }
    }
}
