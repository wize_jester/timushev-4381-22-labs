﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    
    class Article : Publisher
    {
        public string Journal { get; set; }
        public int Number { get; set; }
        public int Year { get; set; }

        public Article(string name, string author, string journal, int num, int year)
        {
            Name = name;
            Author = author;
            Journal = journal;
            Number = num;
            Year = year;
        }

        public override void Print()
        {
            Console.WriteLine("\nСтатья\nНазвание: {0} \nАвтор: {1} \nЖурнал: {2} \nНомер: {3} \nГод издания: {4}", Name, Author, Journal, Number, Year);
        }
    }
}
