﻿using System;

namespace Lab3
{
    class Rectangle : Figs
    {
        public int A { get; set; }
        public int B { get; set; }

        public Rectangle(int a, int b)
        {
            A = a;
            B = b;
        }

        public Rectangle()
        {

        }

        public override double P()
        {
            return 2 * A + 2 * B;
        }
        public override double S()
        {
            return A * B;
        }
        public override void Pr()
        {
            Console.WriteLine("\nПрямоугольник \nА равно: {0} \nВ равно: {1}",A, B);
            Console.WriteLine("Площадь: {0} \nПериметр: {1}",P(), S());
        }
    }
}
