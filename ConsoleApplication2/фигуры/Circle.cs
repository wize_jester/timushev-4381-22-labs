﻿using System;

namespace Lab3
{
    class Circle : Figs
    {
        public int R { get; set; }
        public Circle(int r)
        {
            R = r;
        }
        public override double P()
        {
            return Math.PI * 2 * R;
        }
        public override double S()
        {
            return Math.PI * Math.Pow(R, 2);
        }
        public override void Pr()
        {
            Console.WriteLine("\nОкружность\nРадиус: {0}",R);
            Console.WriteLine("Площадь: {0:f2} \nПериметр: {1:f2}",P(), S());
        } 
    }
}
