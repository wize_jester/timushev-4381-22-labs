﻿using System;

namespace Lab3
{
    class Triangle : Figs
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }
        public Triangle(int a, int b, int c)
        {
            A = a;
            B = b;
            C = c;
        }
        public override double P()
        {
            return A + B + C;
        }
        public override double S()
        {
            double p = P() / 2;
            return Math.Sqrt(p / 2 * (p / 2 - A) * (p / 2 - B) * (p / 2 - C)); ;
        }
        public override void Pr()
        {
            Console.WriteLine("\nТреугольник \nА равно: {0} \nВ равно: {1} \nC равно: {2}", A, B, C);
            Console.WriteLine("Площадь: {0} \nПериметр: {1}", P(), S());
        }

    }
}
