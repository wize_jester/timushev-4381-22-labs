﻿using System;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Figs[] figures = new Figs[3];
            Random R = new Random();
            figures[0] = new Rectangle(R.Next(1,1000), R.Next(1, 1000));
            figures[1] = new Circle(R.Next(1, 1000));
            figures[2] = new Triangle(R.Next(1, 1000), R.Next(1, 1000), R.Next(1, 1000));

            foreach (Figs item in figures)
                item.Pr();

            Funс[] functions = new Funс[3];

            functions[0] = new Line(R.Next(1, 1000), R.Next(1, 1000), R.Next(1, 1000));
            functions[1] = new Kube(R.Next(1, 1000), R.Next(1, 1000), R.Next(1, 1000), R.Next(1, 1000));
            functions[2] = new Hyp(R.Next(1, 1000), R.Next(1, 1000), R.Next(1, 1000));

            foreach (Funс item in functions)
                item.Print();

            Publisher[] publishers = new Publisher[4];

            publishers[0] = new Book("Мы", "Замятин", 2015, "АСТ");
            publishers[1] = new Article("Смерть и психофизические состояния сознания \nво время предсмертной агонии согласно танатологической \nтрадиции буддизма Ваджраяны", "Орлова", "Контекст и рефлексия: \nфилософия о мире и человеке", 1, 2016);
            publishers[2] = new EResource("Google-gravity", "Google", "http://www.mrdoob.com/projects/chromeexperiments/google-gravity/", "Разводим настоящий бардак \nна страницах поисковика Google…");
            publishers[3] = new Book("Полное собрание сочинений \nв одном томе", "Замятин", 2013, "Эксмо");

            foreach (Publisher item in publishers)
                item.Print();

            Console.Write("Введите автора: ");
            string search = Console.ReadLine();

            foreach (Publisher item in publishers)
            {
                if (item.Author == search)
                    item.Print();
            }

            Console.ReadKey();
        }
    }
}//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// Тимушев 4381-22
