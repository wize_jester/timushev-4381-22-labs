﻿using System;

namespace LW5
{
    class Workerhow : Workerfix
    {
        public int Hours { get; set; }

        
        public Workerhow() { }
        public Workerhow(string firstname, string lastname, DateTime dob, char gender, int salary, int bonus, int senioriry, int hours)
            : base(firstname, lastname, dob, gender, salary, bonus, senioriry)
        {
            Hours = hours;
        }
       
        public override string GetInfo()
        {
            return base.GetInfo() + "Hours: " + Hours + '\n';
        }
       
        public override int FinalPay
        {
            get
            {
                return base.FinalPay * Hours;
            }
        }
    }
}
