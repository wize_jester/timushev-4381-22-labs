﻿using System;

namespace LW5
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }

        char gender;
        public char Gender
        {
            get
            {
                return gender;
            }
            set
            {
                if (value == 'M' || value == 'W')
                    gender = value;
                else
                    throw new Exception();
            }
        }
        
        public Person() { }
        public Person(string firstname, string lastname, DateTime dob, char gender)
        {
            FirstName = firstname;
            LastName = lastname;
            Dob = dob;
            Gender = gender;
        }
        
        public int GetAge()
        {
            int age = DateTime.Now.Year - Dob.Year;
            if (!(DateTime.Now.Month > Dob.Month || (DateTime.Now.Month == Dob.Month && DateTime.Now.Day > Dob.Day)))
                age--;
            return age;
        }
       
        public virtual string GetInfo()
        {
            return "Name: " + LastName + " " + FirstName[0] + "., Age: " + GetAge() + ", Gender: " + Gender + '\n';
        }

        public static void GetAllPersons(Person[] collection)
        {
            foreach (Person item in collection)
                Console.WriteLine(item.GetInfo());
        }
        
        public static Person[] SetInfo()
        {
            Console.Write("Person:\n\nNumber of persons: ");
            int num = int.Parse(Console.ReadLine());

            Person[] persons = new Person[num];

            for (int i = 0; i < num; i++)
            {
                persons[i] = new Person();
                Console.WriteLine();

                Console.Write(i + ": First name: ");
                persons[i].FirstName = Console.ReadLine();

                Console.Write(i + ": Last name: ");
                persons[i].LastName = Console.ReadLine();

                Console.Write(i + ": DOB (dd.mm.yyyy): ");
                string[] dobtmp = Console.ReadLine().Split('.');
                persons[i].Dob = new DateTime(int.Parse(dobtmp[2]), int.Parse(dobtmp[1]), int.Parse(dobtmp[0]));

                Console.Write(i + ": Gender: ");
                persons[i].Gender = char.Parse(Console.ReadLine());
            }

            return persons;
        }
    }
}
