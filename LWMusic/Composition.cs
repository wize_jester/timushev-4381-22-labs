﻿using System;
using System.Collections;



namespace LWMusic
{
    class Composition
    {
        public Hashtable Disks;

        public Composition()
        {
            Disks = new Hashtable();
        }

        public void AddDisk(string diskname)
        {
            Disks.Add(diskname, new Hashtable());
        }

        public void RemoveDisk(string diskname)
        {
            Disks.Remove(diskname);
        }

        public void AddSong(string diskname, string songname, string artist)
        {
            Hashtable tmp = (Hashtable)Disks[diskname];
            tmp.Add(songname, artist);
        }

        public void RemoveSong(string diskname, string songname)
        {
            Hashtable tmp = (Hashtable)Disks[diskname];
            tmp.Remove(songname);
        }

        public void PrintComposition()
        {
            ICollection keycomp = Disks.Keys;
            foreach (string ht in keycomp)
            {
                Hashtable vs = (Hashtable)Disks[ht];
                Console.WriteLine("\n" + (new string('-', 50)) + "\n\n" + "Album: " +  ht + '\n');
                ICollection keydisk = vs.Keys;
                foreach (string item in keydisk)
                {
                    Console.WriteLine(vs[item] + " - " + item);
                }

            }
        }

        public void PrintDisk(string diskname)
        {
            Hashtable vs = (Hashtable)Disks[diskname];
            Console.WriteLine("\n" + (new string('-', 50)) + "\n\n" + "Album: " + diskname + '\n');
            ICollection key = vs.Keys;
            foreach (string item in key)
            {
                Console.WriteLine(vs[item] + " - " + item);
            }
        }
        public void SearchArtist(string search)
        {
            ICollection keycomp = Disks.Keys;
            foreach (string ht in keycomp)
            {
                Hashtable vs = (Hashtable)Disks[ht];
                Console.WriteLine("\n" + (new string('-', 50)) + "\n\n" + "Album: " + ht + '\n');
                ICollection keydisk = vs.Keys;
                foreach (string item in keydisk)
                {
                    if ((string)vs[item] == search)
                        Console.WriteLine(vs[item] + " - " + item);
                }

            }
        }
    }
}
